import cv2
import os
import pytesseract as pt

class HTR:
    def __init__(self):
        pass
    
    def _load_image(self,path_to_img):
        img=cv2.imread(path_to_img,0)
        return img
    
    def _preprocess(self,img):
        img = cv2.medianBlur(img,5)
        th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
        return th3
    
    def extract_text(self,path_to_img):
        img=self._load_image(path_to_img)
        if img is not None:
            processed_img=self._preprocess(img)
            cv2.imwrite(path_to_img,processed_img)
            try:
                text=pt.image_to_string(img,lang='htr_eng19_2')
                return text
            except Exception:
                print("Make sure the trained model is placed in the right folder.")
    
    def write_results(self,text,name):
        with open('results/'+name+'.txt','w') as f:
            f.write(text)
        
if __name__=="__main__":
    img_names=os.listdir('test')
    htr=HTR()
    for img_name in img_names:
        print("Loading image {}".format(img_name))
        htr.write_results(htr.extract_text('test/'+img_name),img_name)
