## Add the .traineddata file to tessract-ocr
The .traineddata file is the trained model that recognises handwritten text. It needs to be added to the tessract-ocr's tesstrain folder.
In Linux, it is user/share/tesseract-ocr/4.00/tessdata.

## Place the test images in the test folder
The images that need to be tested need to be placed in the test folder

## Run the htr.py file
To run the script, type python htr.py in the terminal. Once the script is run, the extracted text is placed in the results folder.
